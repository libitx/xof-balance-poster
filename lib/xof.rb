require 'logger'
require 'poloniex'
require 'slack-notifier'

require_relative 'xof/task_helper'
require_relative 'xof/slack_message'

Poloniex.setup do | config |
  config.key    = ENV['POLONIEX_API_KEY']
  config.secret = ENV['POLONIEX_SECRET']
end

module Xof

  VERSION = '0.0.1'

  class PoloniexAuthError < StandardError
  end

  class << self
    def logger
      @logger ||= Logger.new(STDOUT)
    end

    def slack
      @slack ||= Slack::Notifier.new ENV['SLACK_WEBHOOK_URL'], username: 'XOF Bot'
    end
  end

end

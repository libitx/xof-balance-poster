require 'text-table'

module Xof
  class SlackMessage

    def initialize(balances)
      @balances = balances
    end

    def post!
      Xof.slack.ping message
    end

    def message
      <<~EOF
        :slightly_smiling_face: #{ greetings.sample }, XO-Funders!
        I've just taken a snapshot of the fund balance. :moneybag:
        ```
        Total BTC value: #{ '%.8f' % total_balance }
        #{ balance_table.to_s.strip }
        Revision hash: #{ ENV['HEROKU_SLUG_COMMIT'] }
        ```
      EOF
    end

    def total_balance
      @balances.map { |_, v| v['btcValue'].to_f }.inject(:+)
    end

    def balance_table
      Text::Table.new({
        head: [
          'Coin',
          { align: :right, value: 'Total balance' },
          { align: :right, value: 'On orders' },
          { align: :right, value: 'BTC value' }
        ],
        rows: @balances.map do |coin, v|
          [
            coin,
            { align: :right, value: '%.8f' % (v['available'].to_f + v['onOrders'].to_f) },
            { align: :right, value: v['onOrders'] },
            { align: :right, value: v['btcValue'] }
          ]
        end
      })
    end

    def greetings
      %w(
        Ahoj Bonghjornu Bonjour Ciao Cześć
        Hallå Hallo Halló Hello Hei Hej Hi Hola
        Namaste Olà Salam Salaam Sveiki Tere
        Χαίρετε Zdravo
      )
    end

  end
end

module Xof

  module TaskHelper

    def get_balances
      response = Poloniex.post 'returnCompleteBalances', account: 'all'
      balances = JSON.parse(response.body)

      if balances.keys.include?('error')
        raise PoloniexAuthError
      else
        balances.select { |k,v| v['btcValue'].to_f > 0 }
      end
    end

    def post_balances(balances)
      return if balances.empty?
      SlackMessage.new(balances).post!
    end

  end

end
require 'test_helper.rb'

describe Xof do

  describe '.logger' do
    subject { Xof.logger }
    it { subject.must_be_instance_of Logger }
  end

  describe '.slack' do
    subject { Xof.slack }
    it { subject.must_be_instance_of Slack::Notifier }
  end

end

require 'test_helper.rb'

describe Xof::SlackMessage do

  before :all do
    FakeWeb.register_uri(:post, /hooks\.slack\.com/, body: '{"foo":"bar"}')
  end

  subject { Xof::SlackMessage.new balances }
  let(:balances) do
    {
      'ABC' => { 'available' => '10', 'onOrders' => '2', 'btcValue' => '12' },
      'BTC' => { 'available' => '25', 'onOrders' => '5', 'btcValue' => '30' }
    }
  end

  describe '#post!' do
    let(:response) { subject.post! }
    it { response.must_be_kind_of Net::HTTPResponse }
    it { response.body.must_equal '{"foo":"bar"}' }
  end

  describe '#message' do
    it { subject.message.must_be_instance_of String }
    it { subject.message.must_match(/XO\-Funders\!/) }
  end

  describe '#total_balance' do
    it { subject.total_balance.must_be_instance_of Float }
    it { subject.total_balance.must_equal 42 }
  end

  describe '#balance_table' do
    it { subject.balance_table.must_be_instance_of Text::Table }
    it { subject.balance_table.rows.size.must_equal 2 }
  end

  describe '#greetings' do
    it { subject.greetings.must_be_instance_of Array }
    it { subject.greetings.sample.must_be_instance_of String }
  end

end
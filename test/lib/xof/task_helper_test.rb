require 'test_helper.rb'

describe Xof::TaskHelper do

  include Xof::TaskHelper

  describe '#get_balances' do
    before :all do
      FakeWeb.register_uri(:post, 'https://www.poloniex.com/tradingApi', response)
    end

    describe 'when http fails' do
      let(:response) { { exception: Net::HTTPError } }
      it { proc { get_balances }.must_raise Net::HTTPError }
    end

    describe 'when authentication fails' do
      let(:response) { { body: '{"error":"Invalid API key/secret pair."}' } }
      it { proc { get_balances }.must_raise Xof::PoloniexAuthError }
    end

    describe 'when authentication succeeds' do
      let(:response) { { body: '{"BTC":{"available":"25","onOrders":"5","btcValue":"30"}}' } }
      subject { get_balances }
      it { subject.must_be_instance_of Hash }
      it { subject.keys.must_include 'BTC' }
      it { subject['BTC']['btcValue'].must_equal '30' }
    end
  end

  describe '#post_balances' do

    before :all do
      FakeWeb.register_uri(:post, /hooks\.slack\.com/, body: '{"foo":"bar"}')
    end
    
    describe 'when no balances' do
      subject { post_balances({}) }
      it { subject.must_equal nil }
    end

    describe 'when balances' do
      subject { post_balances({ 'BTC' => { 'available' => '25', 'onOrders' => '5', 'btcValue' => '30' } }) }
      it { subject.must_be_kind_of Net::HTTPResponse }
    end
  end

end

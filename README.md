# XOF Balance Poster

A simple Ruby script for posting the XO-Fund balance to Slack.

## Setup

Set the following environment variables:

* POLONIEX_API_KEY
* POLONIEX_SECRET
* SLACK_WEBHOOK_URL

Set a cron job to periodically execute:

    bundle exec rake xof:post_balance

Simples.
